﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
namespace Helpers
{
    [Serializable]
    public enum Attribution {
        [AssetEnumName(AssetEnum.direction)]
        Direction,
        [AssetEnumName(AssetEnum.pipe)]
        Pipe,
        [AssetEnumName(AssetEnum.wall)]
        Wall
    }
    [Serializable]
    public enum Direction { North, East, South, West, Center }

    public static class PhysicsHelper
    {
        static LayerMask LAYER_CLICKABLE = 1 << 8;
        static LayerMask LAYER_DECORATION = 1 << 9;
        static Camera _camera;
        static Camera camera { get { return _camera ?? (_camera = Camera.main); } }
        public static bool TryGetTypeFromRaycast<T>(out T ret) where T : class { return TryGetTypeFromRaycast<T>(LAYER_CLICKABLE, out ret); }
        public static bool TryGetTypeFromRaycast<T>(LayerMask layerMask, out T ret) where T : class
        {
            RaycastHit info;
            ret = null;
#if UNITY_EDITOR
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out info, 100f, layerMask))
#else
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.GetTouch(0).position), out info, 100f, layerMask))
#endif
            {
                return (ret = info.transform.GetComponent<T>()).NonNull();
            }
            return false;
        }
        public static Vector3 ForwardFromDirection(Direction d)
        {
            switch(d)
            {
                case Direction.Center: return Vector3.up;     
                case Direction.North : return Vector3.forward;
                case Direction.East  : return Vector3.right;  
                case Direction.South : return Vector3.back;   
                case Direction.West  : return Vector3.left;   
            }
            return Vector3.zero;
        }
        public static void SetForwardFromDirection(this Transform t, Direction d)
        {
            t.forward = ForwardFromDirection(d);
        }
        public static void SetForwardFromDelta(this Transform t, Vector3 d)
        {
            t.forward = ForwardFromDirection(ConvertDeltaToPrimaryDirection(d));
        }
        public static Direction ConvertDeltaToPrimaryDirection(Vector3 delta, bool allowCenter = false)
        {
            delta = delta.normalized;
            Direction result = Direction.Center;
            float xMag, yMag;
            xMag = delta.x;
            yMag = delta.y;
            if (delta.magnitude == 0) return allowCenter ? Direction.Center : Direction.North;
            else if (Mathf.Abs(xMag) == Mathf.Abs(yMag))
                return ConvertDeltaToPrimaryDirection(delta.x * Vector3.right); //If they're equally horizontal/vertical, take horizontal component
            else if (Mathf.Abs(xMag) > Mathf.Abs(yMag))
                result = xMag > 0 ? Direction.East : Direction.West;
            else if (Mathf.Abs(xMag) < Mathf.Abs(yMag))
                result = yMag > 0 ? Direction.North : Direction.South;
            return result;
        }
        public static void GetDeltaFromDirection(this Direction dir, out int dX, out int dZ)
        {
            dX = dZ = 0;
            switch(dir)
            {
                case Direction.West:    dX -= 1; break;
                case Direction.East:    dX += 1; break;
                case Direction.North:   dZ += 1; break;
                case Direction.South:   dZ -= 1; break;
            }
        }
        public static Direction Complement(this Direction dir)
        {
            switch (dir)
            {
                case Direction.West: return Direction.East;
                case Direction.East: return Direction.West;
                case Direction.North: return Direction.South;
                case Direction.South: return Direction.North;
                default: return Direction.Center;
            }
        }
        internal static void Childify(Transform parent, Transform child)
        {
            child.parent = parent;
            child.transform.localPosition = Vector3.up * 0.1f;
        }
    }

    public static class AssetHelper
    {
        private static Dictionary<AssetEnum, UnityEngine.Object> cachedObjects = new Dictionary<AssetEnum, UnityEngine.Object>();
        public static T GetCachedObject<T>(AssetEnum key) where T : UnityEngine.Object
        {
            if (!cachedObjects.ContainsKey(key))
                cachedObjects.Add(key, Resources.Load<T>(key.GetPath()));
            return (cachedObjects[key] as T);
        }
        public static GameObject GetCachedGameObject(AssetEnum key)
        {
            GameObject ret = GetCachedObject<GameObject>(key);
            return GameObject.Instantiate(ret);
        }
        public static GameObject GetCachedBillboard(AssetEnum key, out Material mat)
        {
            GameObject go = GetCachedGameObject(AssetEnum.billboard);
            mat = Material.Instantiate(GetCachedObject<Material>(key));
            go.GetComponentInChildren<MeshRenderer>().material = mat;
            return go;
        }
        public static GameObject GetCachedBillboard(AssetEnum key)
        {
            Material mat; //ignore the material, if you want to. Otherwise use the overload.
            return GetCachedBillboard(key, out mat);
        }
        public static string GetPath(this Enum value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    Path attr = Attribute.GetCustomAttribute(field, typeof(Path)) as Path;
                    if (attr.NonNull()) return attr.FilePath;
                }
            }
            return null;
        }
        public static AssetEnum GetAssetName(this Attribution value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    AssetEnumName attr = Attribute.GetCustomAttribute(field, typeof(AssetEnumName)) as AssetEnumName;
                    if (attr.NonNull()) return attr.AssetEnum;
                }
            }
            return AssetEnum.none;
        }
    }
    public class Path : System.Attribute
    {
        public string FilePath { get; private set; }
        public Path(string fp) { FilePath = fp; }
    }
    public enum AssetEnum
    {
        [Path("billboard")]
        billboard,
        [Path("Art/dir")]
        direction,
        [Path("Art/pipe")]
        pipe,
        [Path("Art/wall")]
        wall,
        none
    }
    public class AssetEnumName : System.Attribute
    {
        public AssetEnum AssetEnum { get; private set; }
        public AssetEnumName(AssetEnum a) { AssetEnum = a; }
    }
    public static class Extensions
    {
        public static bool IsNull(this object o) { return o == null; }
        public static bool NonNull(this object o) { return o != null; }
        public static bool IsEmpty(this string s) { return s == ""; }
        public static Coroutine Fade(this Material m, Color a, Color b, float fadeTime)
        {
            return MonoExtension.instance.Fade(m, a, b, fadeTime);
        }
    }
    public class MonoExtension : MonoBehaviour
    {
        private static MonoExtension _instance;
        public static MonoExtension instance
        {
            get
            {
                return _instance ?? (_instance = new GameObject().AddComponent<MonoExtension>());
            }
        }
        public void Awake() { transform.name = "MonoExtension"; }
        public Coroutine Fade(Material m, Color a, Color b, float fadeTime)
        {
            return StartCoroutine(FadeIE(m, a, b, fadeTime));
        }
        public IEnumerator FadeIE(Material m, Color a, Color b, float fadeTime)
        {
            float ft = 0;
            while (ft < fadeTime)
            {
                m.color = Color.Lerp(a, b, ft / fadeTime);
                ft += Time.deltaTime;
                yield return null;
            }
            m.color = b;
            yield break;
        }
    }
}
