﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using Helpers.Xml;
using Helpers;

public class Grid : MonoBehaviour
{
    [SerializeField] int x;
    [SerializeField] int z;
    [SerializeField] GameObject cellPrefab;
    [SerializeField] string xmlPath;
    [SerializeField] XmlStyle xmlStyle;
    protected XmlCellContainer container = new XmlCellContainer();
    public XmlCellContainer GetContainer() { return container; }
    protected void Start()
    {
        CreateGrid();
        StartCoroutine(Engine.instance.Loop(this));
    }
    protected void CreateGrid()
    {
        switch (xmlStyle)
        {
            case XmlStyle.ReadOnly:
                CreateGridFromXmlPath();
                break;
            case XmlStyle.WriteOnly:
                CreateGridFromScratch();
                break;
            case XmlStyle.ReadOrWriteIfNonExistant:
                if (File.Exists(xmlPath))
                    CreateGridFromScratch();
                else
                    CreateGridFromXmlPath();
                break;
        }
    }
    protected void CreateGridFromScratch()
    {
        for (int iz = 0; iz < z; iz++)
        {
            for(int ix = 0; ix < x; ix++)
            {
                XmlCell xCell = container.AddCell(ix, iz);
                CellFromXZ(ix, iz, xCell);
            }
        }
        if (xmlPath.IsEmpty()) return;
        XmlHelper.Serialize(xmlPath, container);
    }
    protected void CreateGridFromXmlPath()
    {
        XmlHelper.Deserialize<XmlCellContainer>(xmlPath, out container);
        foreach(XmlCell xCell in container.cells)
        {
            int ix, iz;
            ix = xCell.x;
            iz = xCell.z;
            CellFromXZ(ix, iz, xCell);
        }
    }
    protected Cell CellFromXZ(int ix, int iz, XmlCell xCell)
    {
        Transform tCell = (GameObject.Instantiate(cellPrefab) as GameObject).transform;
        Cell cCell = tCell.GetComponent<Cell>();
        tCell.parent = transform;
        tCell.localPosition = new Vector3(ix, 0, iz);
        tCell.name = String.Format("{0}:{1}",ix,iz);
        cCell.xCell = xCell;
        xCell.RemakeAttributes(tCell);
        xCell.cCell = cCell;
        return cCell;
    }

    XmlCellContainer saveContainer;
    public void QueueSave(XmlCellContainer container)
    {
        saveContainer = container;
    }
    public void OnDestroy()
    {
        if(saveContainer.NonNull())
            XmlHelper.Serialize(xmlPath+"[save]", saveContainer);
    }
}
