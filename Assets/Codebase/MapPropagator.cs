﻿using Helpers;
using Helpers.Xml;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapPropagator : Engine.BaseGridModifier
{
    public override IEnumerator OnTick()
    {
        Debug.Log(Time.time);
        if (dictionary.Count == 0)
            dictionary.Add(
                getContainer().cells[UnityEngine.Random.Range(0, getContainer().cells.Count)],
                PropogationColor.Blue
                );
        foreach(KeyValuePair<XmlCell, PropogationColor> kvp in dictionary)
        {
            kvp.Key.cCell.GetComponentInChildren<MeshRenderer>().material.color = Color.cyan;
            AllCellsInAround(kvp.Key, getContainer()).FindAll(e => !dictionary.ContainsKey(e)).
                ForEach(c => dictionary.Add(c, PropogationColor.Blue));
        }
        yield break;
    }
    public enum PropogationColor { Red, Blue }
    Dictionary<XmlCell, PropogationColor> dictionary = new Dictionary<XmlCell, PropogationColor>();
    List<XmlCell> AllCellsInAround(XmlCell center, XmlCellContainer container)
    {
        List<XmlCell> result = new List<XmlCell>();
        for (int ix = -1; ix <= 1; ix++)
        {
            for (int iz = -1; iz <= 1; iz++)
            {
                if (ix == iz) continue;
                if (Mathf.Abs(ix * iz) == 1) continue;
                result.Add(center);
                container.TryGetCellAtCellDelta(center, ix, iz, 
                    (XmlCell xCell) => { return xCell.NonNull(); }
                    );
            }
        }
        return result;
    }
}
