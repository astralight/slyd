﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace Helpers.Xml
{
    public class XmlHelper
    {
        public static string ValidateXmlPath(string path)
        {
            if (!path.EndsWith(".xml"))
                path += ".xml";
            return path;
        }
        public static void Serialize(string xmlPath, object obj)
        {
            xmlPath = ValidateXmlPath(xmlPath);
            var serializer = new XmlSerializer(typeof(XmlCellContainer));
            var stream = new FileStream(xmlPath, FileMode.Create);
            serializer.Serialize(stream, obj);
            stream.Close();
        }
        public static void Deserialize<T>(string xmlPath, out T obj) where T : class
        {
            xmlPath = ValidateXmlPath(xmlPath);
            var serializer = new XmlSerializer(typeof(XmlCellContainer));
            var stream = new FileStream(xmlPath, FileMode.Open);
            obj = null;

            try { obj = (T)serializer.Deserialize(stream); }
            catch (Exception e) { Debug.LogError(e.Message); }
            finally { stream.Close(); }
        }
    }
    [XmlRoot("CellContainer")]
    public class XmlCellContainer
    {
        [XmlArray("Cells")]
        [XmlArrayItem("Cell")]
        public List<XmlCell> cells = new List<XmlCell>();
        public XmlCell AddCell(int ix, int iz, params XmlAttribution[] attributes)
        {
            XmlCell xCell = new XmlCell() { x = ix, z = iz, attributes = new List<XmlAttribution>(attributes) };
            cells.Add(xCell);
            return xCell;
        }
        public XmlCell TryGetCellAtCellDelta(XmlCell center, int dx, int dz, Func<XmlCell, bool> CheckFunc)
        {
            XmlCell cell;
            cell = cells.FirstOrDefault(x => x.x == center.x + dx && x.z == center.z + dz);
            if (cell.NonNull() && CheckFunc(cell))
                return cell;
            return null;
        }
    }
    public class XmlCell
    {
        [XmlAttribute("X")]
        public int x;
        [XmlAttribute("Z")]
        public int z;
        [XmlArray("Attributions")]
        [XmlArrayItem("Attribution")]
        public List<XmlAttribution> attributes = new List<XmlAttribution>();
        [XmlIgnore]
        public Cell cCell;
        internal void AddAttribute(Attribution attribution, Direction direction)
        {
            XmlAttribution xa = new XmlAttribution { attribution = attribution, direction = direction };
            attributes.Add(xa);
        }
        public List<GameObject> RemakeAttributes(Transform parent)
        {
            List<GameObject> xAttributes = new List<GameObject>();
            foreach(XmlAttribution xa in attributes)
            {
                if (xa.billboard)
                    GameObject.Destroy(xa.billboard);
                xa.billboard = AssetHelper.GetCachedBillboard(xa.attribution.GetAssetName());
                xa.billboard.transform.forward = PhysicsHelper.ForwardFromDirection(xa.direction);
                xAttributes.Add(xa.billboard);
                PhysicsHelper.Childify(parent, xa.billboard.transform);
            }
            return xAttributes;
        }
        public Vector3 GetPosition()
        {
            return new Vector3(x, 0, z);
        }
        public void Delta(int dx, int dz)
        {
            x += dx;
            z += dz;
        }
    }
    public class XmlAttribution
    {
        [XmlAttribute("attribution")]
        public Attribution attribution;
        [XmlAttribute("direction")]
        public Direction direction;
        [XmlIgnore]
        public GameObject billboard;
    }
}
public enum XmlStyle { ReadOnly, WriteOnly, ReadOrWriteIfNonExistant }