﻿using Helpers;
using Helpers.Xml;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSlider : Engine.BaseGridModifier
{
    public enum SlideStyle { AllForward, AllBackward, AllInLine}
    const SlideStyle style = SlideStyle.AllInLine;
    public override IEnumerator OnClickCell(Cell c, int button)
    {
        Engine.Elf elf = new Engine.TryPullCellElf(c, true);
        yield return elf.Func();
        Direction d = (elf as Engine.TryPullCellElf).GetResult();
        XmlCellContainer container = getContainer();
        int deltaX, deltaZ;
        d.GetDeltaFromDirection(out deltaX, out deltaZ);
        List<XmlCell> line = new List<XmlCell>();
        XmlCell center = c.xCell;
        if (style == SlideStyle.AllForward  || style == SlideStyle.AllInLine)
            line.AddRange(AllCellsInDirection(center, d,              container));
        if (style == SlideStyle.AllBackward || style == SlideStyle.AllInLine)
            line.AddRange(AllCellsInDirection(center, d.Complement(), container));
        line = line.Distinct().ToList();
        yield return MonoExtension.instance.StartCoroutine(Engine.RefreshAllCellPositions(line, deltaX, deltaZ, 0.25f));
    }
    List<XmlCell> AllCellsInDirection(XmlCell center, Direction d, XmlCellContainer container)
    {
        List<XmlCell> result = new List<XmlCell>();
        int deltaX, deltaZ;
        d.GetDeltaFromDirection(out deltaX, out deltaZ);
        while (center != null)
        {
            result.Add(center);
            XmlCell next = null;
            next = container.cells.SingleOrDefault(e => e.x == center.x + deltaX && e.z == center.z + deltaZ && !e.attributes.Any(a => a.direction == d.Complement() && a.attribution == Attribution.Wall));
            center = next;
        }
        return result;
    }
}
