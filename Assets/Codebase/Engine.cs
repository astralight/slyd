﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Helpers;
using Helpers.Xml;

public class Engine : MonoBehaviour {
    private static Engine _instance;
    public static Engine instance { get { return _instance ?? (_instance = new GameObject().AddComponent<Engine>()); } }
    [SerializeField] internal float tickRate = 0.5f;
    [SerializeField] internal List<BaseGridModifierTypes> modifierTypes;
    protected internal List<BaseGridModifier> modifiers = new List<BaseGridModifier>();
    public void Awake()
    {
        _instance = this;
        //Factory all of the /modifierTypes/, and place them into /modifiers/
        modifiers.AddRange(modifierTypes.Select(t => BaseGridModifier.Factory(t, GetContainer, ForceRefreshCell, ForceRefreshContainer)).Where(e => e.NonNull()));
    }
    XmlCellContainer GetContainer() { return grid.GetContainer(); }
    Grid grid;
    public IEnumerator Loop (Grid grid_) {
        grid = grid_;
        StartCoroutine(Ticks());
        while (true)
        {
            yield return Waits();
            yield return null;
        }
	}
    public IEnumerator Ticks()
    {
        float t = 0;
        while(true)
        {
            float sPerT = 1f / tickRate;
            while (t < sPerT)
            {
                yield return null;
                t += Time.deltaTime;
            }
            foreach (BaseGridModifier mod in modifiers)
                StartCoroutine(mod.OnTick());
            t = 0;
        }
    }
    protected IEnumerator Waits()
    {
        //provide a callback?
        yield return WaitForClick();
        //StartCoroutine(WaitForKeyCode(KeyCode.Space));
        yield break;
    }
    protected IEnumerator WaitForClick()
    {
#if UNITY_EDITOR
        while(!Input.GetMouseButtonDown(0))
#else
        while(Input.touchCount == 0)
#endif
            yield return null;
        Cell cell;
        if(PhysicsHelper.TryGetTypeFromRaycast<Cell>(out cell))
        {
            foreach(BaseGridModifier mod in modifiers)
            {
                yield return StartCoroutine(mod.OnClickCell(cell));
            }
        }
        yield return null;
    }
    protected IEnumerator WaitForKeyCode(KeyCode kc)
    {
        while (!Input.GetKeyDown(kc))
            yield return null;
        yield break;
    }
    public void ForceRefreshCell(Cell c)
    {
        c.xCell.RemakeAttributes(c.transform);
    }
    public void ForceRefreshContainer(XmlCellContainer c)
    {
        if (c.IsNull()) c = GetContainer();
        grid.QueueSave(c);
    }
    public static IEnumerator RefreshAllCellPositions(List<XmlCell> list, int deltaX, int deltaZ, float moveTime)
    {
        HashSet<XmlCell> waitingOn = new HashSet<XmlCell>(list);
        foreach(XmlCell l in list)
        {
            XmlCell me = l;
            MonoExtension.instance.StartCoroutine(
                RefreshCellPosition(l, deltaX, deltaZ, moveTime, ()=> { waitingOn.Remove(me); })
                );
        }
        while (waitingOn.Count != 0)
            yield return null;
    }
    public static IEnumerator RefreshCellPosition(XmlCell cell, int deltaX, int deltaZ, float moveTime, Action callback)
    {
        Vector3 a, b;
        a = cell.GetPosition();
        cell.Delta(deltaX, deltaZ);
        b = cell.GetPosition();
        float t = 0;
        while (t < moveTime)
        {
            t += Time.deltaTime;
            cell.cCell.transform.position = Vector3.Lerp(a, b, t/moveTime);
            yield return null;
        }
        callback();
    }
    public abstract class Elf
    {
        protected object Result { get; set; }
        public virtual T GetResult<T>() where T : class
        {
            Debug.Assert(Result.GetType() == typeof(T));
            return Result as T;
        }
        public abstract IEnumerator Func();
    }
    public class TryPullCellElf : Elf
    {
        private Cell focusCell;
        private bool prettify;
        public TryPullCellElf (Cell c, bool prettify)
        {
            focusCell = c;
            this.prettify = prettify;
        }
        public override IEnumerator Func()
        {
            Vector3 mouseInputStart = Input.mousePosition;
            const float breakawayDistance = 50;
            //wait until we have broken away
            Material mat;
            Transform dirHelper = AssetHelper.GetCachedBillboard(Attribution.Direction.GetAssetName(), out mat).transform;
            PhysicsHelper.Childify(focusCell.transform, dirHelper);
            Vector3 delta;
            float magnitude;
            if(prettify)
                mat.Fade(Color.clear, Color.white, 0.1f);
            while((magnitude = (delta = Input.mousePosition-mouseInputStart).magnitude) < breakawayDistance)
            {
                dirHelper.transform.localScale = Vector3.one * Mathf.Lerp(0, 1, magnitude / breakawayDistance);
                dirHelper.SetForwardFromDelta(delta);
                yield return null;
            }
            if (prettify)
                mat.Fade(Color.white, Color.clear, 0.1f);
            GameObject.Destroy(dirHelper.gameObject);
            Result = PhysicsHelper.ConvertDeltaToPrimaryDirection(delta);
            yield break;
        }
        public Direction GetResult()
        {
            return (Direction)Result;
        }
    }
    [Serializable]
    public class BaseGridModifier : ScriptableObject
    {
        public virtual IEnumerator OnClickCell(Cell c, int button = 0) { yield break; }
        public virtual IEnumerator OnTick() { yield break; }
        ///To be able to factory a type, you need to add it to this.. Too bad.///
        public static BaseGridModifier Factory(BaseGridModifierTypes modT, Func<XmlCellContainer> getContainer, Action<Cell> forceRefreshCell_, Action<XmlCellContainer> forceRefreshContainer_)
        {
            BaseGridModifier mod;
            switch (modT)
            {
                case BaseGridModifierTypes.MapEditor:
                    mod = BaseGridModifier.Factory<MapEditor>();
                    break;
                case BaseGridModifierTypes.MapSlider:
                    mod = BaseGridModifier.Factory<MapSlider>();
                    break;
                case BaseGridModifierTypes.MapPropagator:
                    mod = BaseGridModifier.Factory<MapPropagator>();
                    break;
                default: return null;
            }
            mod.getContainer = getContainer;
            mod.forceRefreshCell = forceRefreshCell_;
            mod.forceRefreshContainer = forceRefreshContainer_;
            return mod;
        }
        protected Func<XmlCellContainer> getContainer;
        protected Action<Cell> forceRefreshCell;
        protected Action<XmlCellContainer> forceRefreshContainer;
        public static BaseGridModifier Factory<T>() where T : BaseGridModifier{
            return BaseGridModifier.CreateInstance<T>();
        }
    }
    public enum BaseGridModifierTypes
    {
        MapEditor,
        MapSlider,
        MapPropagator
    }
}