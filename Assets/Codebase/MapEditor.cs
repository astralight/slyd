﻿using Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapEditor : Engine.BaseGridModifier
{
    public override IEnumerator OnClickCell(Cell c, int button)
    {
        Engine.Elf elf = new Engine.TryPullCellElf(c, true);
        yield return elf.Func();
        Attribution attribute = Attribution.Wall;
        Direction d = (elf as Engine.TryPullCellElf).GetResult();
        c.xCell.AddAttribute(attribute, d);
        forceRefreshCell(c);
        forceRefreshContainer(null);
    }
}
